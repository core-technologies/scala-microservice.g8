# Scala Microservice

Template for a Docker Scala microservice based on [Giter8](http://www.foundweekends.org/giter8/)

## Create a new project
Use the following command to create a new project based on this template:
```
g8 https://gitlab.com/core-technologies/scala-microservice.g8.git
```
If you want to use a specific branch use:
```
g8 -b <branch> https://gitlab.com/core-technologies/scala-microservice.g8.git
```
### Authentication
You need to authenticate with your Gitlab credentials.

In case you activated MFA in Gitlab you need to generate an access token to authenticate:
1. Login to Gitlab and click on your avatar
2. Settings -> Access Tokens
3. Enter `giter8` for name and tick `read_repository`
4. Create personal access token
5. Store the contents of field `Your New Personal Access Token`. This value needs to be used as password.

## Local testing
```
g8 file://scala-microservice.g8/ --name=my-service --force
```

