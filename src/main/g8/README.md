# $name$

Please replace this with a short description of this microservice.

## Before you push

Before you push to remote please run automatic code formatting and static code analysis by executing this command (
this will also be checked as part of the CI pipeline):
```
sbt "scalafix --check" scalafmtCheck scapegoat
```   

## Logging

By default log output is in JSON format because this is easier to process at 
runtime. If you want to switch to the pattern appender during development for human-readable output 
just set the following system environment variable (e.g. in IntelliJ Run Configuration): 
```
LOG_APPENDER=pattern
```

## Code formatting

We use [Scalafmt](https://scalameta.org/scalafmt) to enforce code formatting rules across projects.
IntelliJ will automatically use it to format the codebase (e.g. using shortcut `⌥ + ⌘ + L`). You can
also use SBT to format the codebase:
```
sbt scalafmt
```

## Linting and static code analysis

We are using the following tools to ensure common code style best practises (those tools are integrated
in the SBT build and CI pipeline):

- [WartRemove](https://www.wartremover.org)
- [Scalafix](https://scalacenter.github.io/scalafix)
- [Scapegoat](https://github.com/sksamuel/scapegoat)

## Environment Variables

| Environment variable | Description | Sample value |
| --- | --- | ---  |
| LOG_LEVEL | Defines the log level for loggers starting with `com.eon` | info |
| LOG_APPENDER | Defines the log format (can be `pattern` for human-readable and `json` for machine-readable output) | pattern |
