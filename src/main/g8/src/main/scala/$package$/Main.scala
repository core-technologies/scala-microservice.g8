package $package$

import org.apache.logging.log4j.LogManager

object Main extends App {
  private val logger = LogManager.getLogger(getClass.getName)

  logger.info(BuildInfo.toString)
}
